#!/bin/bash

source /opt/ros/indigo/setup.bash
. ~/catkin_ws/devel/setup.bash
export EDITOR='nano -w'
export ROS_LANG_DISABLE=genlisp
# export ROSLAUNCH_SSH_UNKNOWN=1
# Depends on the raspberrypi having a static IP (/etc/dhcpcd.conf)
# and an entry in the local /etc/hosts to map the IP to the host name
export ROS_HOSTNAME='badmonkey'
export ROS_MASTER_URI=http://raspberrypi:11311
# Update path to Python to support PyQt
PYTHONPATH="${PYTHONPATH}:/usr/lib/x86_64-linux-gnu"
PYTHONPATH="${PYTHONPATH}:/home/mike/catkin_ws/src/inmoov/src/inmoov"
export PYTHONPATH
exec "$@"
