# InMoov for the Robot Operating System
This is a personal project to implement the [InMoov 3-D printed robot](inmoov.fr) using:

* [Robot Operating System](ros.org)
* [Raspberry Pi](raspberrypi.org)

For more detailed information, visit [Robotic Dreams](roboticdreams.wordpress.com).  Several posts explain many of the code details.

# Architecture
The ROS node architecture is depicted below.

![InMoov ROS Nodes](docs/architecture.png)

* **Hotword** - Detects the name of the robot ('Jarvis') to start a conversation
* **Conversation** - Manages an ongoing conversation and unsolicated status updates due to external events
* **STT** - Speech to Text, implemented with CMU Sphinx and Pocket Sphinx
* **NLP** - Natural Language Processing, implemented with [api.ai](http://www.api.ai)
* **Commands** - routes NLP results to appropriate component for action
* **LeapMotionPublisher** - connects to another laptop to stream Leap Motion controller data
* **LeapMotionSubscriber** - interprets Leap Motion controller data stream into Hand servo positions
* **Hand** - the ROS action server that is responsible for positioning a hand
* **LogMonitor** - a useful log aggregator for nodes that span across multiple computers

# License

Copyright (c) 2016-2017 Mike Jacobs

Permission is hereby granted, free of charge, to any person obtaining 
a copy of this software and associated documentation files 
(the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, 
publish, distribute, sublicense, and/or sell copies of the Software, 
and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
