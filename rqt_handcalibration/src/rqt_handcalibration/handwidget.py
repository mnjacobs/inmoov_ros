import os
import rospy
import rospkg
import actionlib

from qt_gui.plugin import Plugin
from python_qt_binding import loadUi
from python_qt_binding.QtGui import QWidget
from inmoov.msg import HandAction, HandGoal, HandResult

class HandCalibrationPlugin(Plugin):

    def __init__(self, context):
        super(HandCalibrationPlugin, self).__init__(context)
        # Give QObjects reasonable names
        self.setObjectName('HandCalibrationPlugin')

        # Process standalone plugin command-line arguments
        from argparse import ArgumentParser
        parser = ArgumentParser()
        # Add argument(s) to the parser.
        parser.add_argument("-q", "--quiet", action="store_true",
                      dest="quiet",
                      help="Put plugin in silent mode")
        args, unknowns = parser.parse_known_args(context.argv())
        if not args.quiet:
            print 'arguments: ', args
            print 'unknowns: ', unknowns

        # Create QWidget
        self._widget = QWidget()
        # Get path to UI file which should be in the "resource" folder of this package
        ui_file = os.path.join(rospkg.RosPack().get_path('rqt_handcalibration'), 'resource', 'handcalibrationwidget.ui')
        # Extend the widget with all attributes and children from UI file
        loadUi(ui_file, self._widget)
        # Give QObjects reasonable names
        self._widget.setObjectName('HandCalibrationWidgetx')
        # Show _widget.windowTitle on left-top of each plugin (when 
        # it's set in _widget). This is useful when you open multiple 
        # plugins at once. Also if you open multiple instances of your 
        # plugin at once, these lines add number to make it easy to 
        # tell from pane to pane.
        self.setUpEventHandlers()
        
        if context.serial_number() > 1:
            self._widget.setWindowTitle(self._widget.windowTitle() + (' (%d)' % context.serial_number()))
        # Add widget to the user interface
        context.add_widget(self._widget)
        self.connectToServer()
            
    def connectToServer(self):
        self.handActionClient = actionlib.SimpleActionClient('inmoov/right/hand', HandAction)
        rospy.loginfo("Waiting for hand action server...")
        self.handActionClient.wait_for_server()
        rospy.loginfo("Hand action server found")
        self.goal = HandGoal()
        
    def setUpEventHandlers(self):
        self._widget.wristSlider.valueChanged.connect(self.handleWristChange)
        self._widget.thumbSlider.valueChanged.connect(self.handleThumbChange)
        self._widget.indexSlider.valueChanged.connect(self.handleIndexChange)
        self._widget.middleSlider.valueChanged.connect(self.handleMiddleChange)
        self._widget.ringSlider.valueChanged.connect(self.handleRingChange)
        self._widget.pinkySlider.valueChanged.connect(self.handlePinkyChange)
        
    def shutdown_plugin(self):
        # TODO unregister all publishers here
        pass

    def save_settings(self, plugin_settings, instance_settings):
        # TODO save intrinsic configuration, usually using:
        # instance_settings.set_value(k, v)
        pass

    def restore_settings(self, plugin_settings, instance_settings):
        # TODO restore intrinsic configuration, usually using:
        # v = instance_settings.value(k)
        pass
        
    #def trigger_configuration(self):
        # Comment in to signal that the plugin has a way to configure
        # This will enable a setting button (gear icon) in each dock widget title bar
        # Usually used to open a modal configuration dialog
        
    def handleWristChange(self, value):
        self.goal.wrist = value
        self.handActionClient.send_goal(self.goal)
        
    def handleThumbChange(self, value):
        self.goal.thumb = value
        self.handActionClient.send_goal(self.goal)
        
    def handleIndexChange(self, value):
        self.goal.index = value
        self.handActionClient.send_goal(self.goal)
        
    def handleMiddleChange(self, value):
        self.goal.middle = value
        self.handActionClient.send_goal(self.goal)
        
    def handleRingChange(self, value):
        self.goal.ring = value
        self.handActionClient.send_goal(self.goal)
        
    def handlePinkyChange(self, value):
        self.goal.pinky = value
        self.handActionClient.send_goal(self.goal)


