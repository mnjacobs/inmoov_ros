#!/bin/bash

rsync -p -v -r -e ssh --delete --exclude ".*/" --exclude "*.pyc" /home/mike/catkin_ws/src/inmoov pi@raspberrypi:/home/pi/catkin_ws/src
#rsync -p -v -r -e ssh --delete --exclude ".*/" --exclude "*.pyc" /home/mike/catkin_ws/src/pocketsphinx pi@raspberrypi:/home/pi/catkin_ws/src
rsync -v -r -e ssh --delete --exclude ".*/" --exclude "*.pyc" /home/mike/catkin_ws/src/rqt_handcalibration pi@raspberrypi:/home/pi/catkin_ws/src
