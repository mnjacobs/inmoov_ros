#!/usr/bin/python
import websocket
import time
import datetime
import json

class WebSocketClient(object):
    def __init__(self, url):
        websocket.enableTrace(True)
        self.ws = websocket.WebSocketApp(url,
                                on_open = self.on_open,
                                on_message = self.on_message,
                                on_error = self.on_error,
                                on_close = self.on_close)
        self.frameCount = 0
        self.startTime = 0
        self.endTime = 0
                                
    def on_message(self, ws, message):
        # message is a string
        dictionary = json.loads(message)
        if "hands" in dictionary:
            for hand in dictionary["hands"]:
                print('Hand: {0}'.format(hand))
            else: print 'no hand'
        #self.frameCount = self.frameCount + 1
        #numberOfFrames = 100
        #if(self.frameCount % numberOfFrames == 0):
        #   self.endTime = datetime.datetime.now()
        #   duration = self.endTime - self.startTime
        #   frameRate = 1.0*numberOfFrames/(1.0*(duration.total_seconds()))
        #   print('Frame rate: {0}/t{1}'.format(frameRate, duration))
        #   self.startTime = self.endTime

    def on_error(self, ws, error):
        print error

    def on_close(self, ws):
        print "### closed ###"

    def on_open(self, ws):
        print "### opened ###"
        backgroundJSON = json.dumps({"background":True})
        print('Sending: {0}'.format(backgroundJSON))
        self.ws.send(backgroundJSON)
    
    def run_forever(self):
        self.startTime = datetime.datetime.now()
        self.ws.run_forever()

if __name__ == "__main__":
    
    client = WebSocketClient("ws://MacBook-Pro-2.local:6437/v6.json")
    client.run_forever()
