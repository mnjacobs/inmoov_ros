#! /usr/bin/env python

# Copyright (c) 2016-2017 Mike Jacobs
# 
# Permission is hereby granted, free of charge, to any person obtaining 
# a copy of this software and associated documentation files 
# (the "Software"), to deal in the Software without restriction, 
# including without limitation the rights to use, copy, modify, merge, 
# publish, distribute, sublicense, and/or sell copies of the Software, 
# and to permit persons to whom the Software is furnished to do so, 
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
import rospy
import math
import actionlib
from inmoov.msg import HandAction, HandGoal, HandResult
from inmoov.msg import LeapFrame, LeapHand, LeapPointable, LeapGesture
from geometry_msgs.msg import Point, Vector3

FINGERS = ['thumb', 'index', 'middle', 'ring', 'pinky'] #pointable.pointableType
INDEX = 1
RIGHT = 'right'
subscriber = None

def magnitude(v):
    # Leap vectors appear to be normalized, so this is not needed for dot
    return math.sqrt(v.x*v.x + v.y*v.y + v.z*v.z) 
    
def dot(v1, v2):
    # dot product of two vector messages
    return (v1.x*v2.x + v1.y*v2.y + v1.z*v2.z)

def angleRadians(v1, v2):
    # angle between two vectors in radians
    # Leap vectors appear to be normalized
    return math.acos(dot(v1,v2))
    
def angleDegrees(v1, v2):
    # angle between two vectors in degrees
    return math.degrees(angleRadians(v1, v2))
    
def cross(v1, v2):
    # Cross product of two vectors v = v1 X v2
    # Uses Sarrus' rule or cofactor expansion
    v = Vector3()
    v.x = v1.y * v2.z - v1.z * v2.y
    v.y = v1.z * v2.x - v1.x * v2.z
    v.z = v1.x * v2.y - v1.y * v2.x
    return v
    
def handleMessage(frame):
    hands = frame.hands
    pointables = frame.pointables
    #rospy.loginfo('{0} : Leap message received with {1} hands and {2} fingers'.format(frame.header.seq, len(hands), len(pointables)))
    
    for hand in hands:
        now = rospy.Time.now()
        confidence = hand.confidence
        if(confidence < 0.50): 
            return
        
        handId = hand.ID
        handDirection = hand.direction
        palmNormal = hand.palmNormal
        handGoal = HandGoal()
        # Fingers
        for pointable in pointables:
            if((pointable.handId == handId) and (pointable.tool == False)):
                # This is a finger for the current hand and not a tool
                reference = handDirection
                if(FINGERS[pointable.pointableType] == 'thumb'):
                    reference = cross(handDirection, palmNormal)
                angle = angleDegrees(reference, pointable.direction)
                if((angle < 30) and pointable.extended): 
                    # Let Leap decide if a finger is completely extended
                    angle = 0
                setattr(handGoal, FINGERS[pointable.pointableType], int(angle))
        
        # Wrist - Palm facing down is considered 90 degrees, turned with thumb up is 0 degrees
        # Use the palm normal compared to the x-axis to determine rotation
        downReference = Vector3()
        if(hand.handType == RIGHT):
            downReference.x = -1  # negative x-axis
        else:
            downReference.x = 1  # positive x-axis
        downReference.y = 0
        downReference.z = 0
        handGoal.wrist = int(angleDegrees(downReference, palmNormal))
        #rospy.loginfo("Wrist angle: {0}".format(handGoal.wrist))
                        
        handGoal.header.stamp = now
        handGoal.header.seq = frame.header.seq
        handGoal.received = frame.header.stamp
        handGoal.created = frame.timestamp
        
        if(hand.handType == RIGHT):
            rightHand.send_goal(handGoal)
        #else: 
            # rospy.loginfo("*** LEFT HAND NOT IMPLEMENTED YET ***")
        
 
def onShutdown():
    rospy.logdebug("Leap Subscriber shutting down")
    global subscriber
    if(subscriber is not None):
        subscriber.unregister()
    rospy.sleep(5.0)
    
rospy.init_node('LeapMotionSubscriber')
rospy.on_shutdown(onShutdown)
rightHand = actionlib.SimpleActionClient('right/hand', HandAction)
rospy.logdebug("Waiting for right hand action server...")
rightHand.wait_for_server()
rospy.logdebug("Right hand action server found")
subscriber = rospy.Subscriber('leapmotion', LeapFrame, handleMessage, queue_size = 1)
rospy.logdebug("Leap Motion Subscriber SUCCESSFULLY started")

try:
    rospy.spin()
except:
    rospy.logwarn("Leap Motion Subscriber FAILED")
    import sys, traceback
    exc_type, exc_value, exc_traceback = sys.exc_info()
    traceback.print_exception(exc_type, exc_value, exc_traceback, limit=5, file=sys.stdout)
