#!/usr/bin/env python
# Copyright (c) 2017 Mike Jacobs
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from pocketsphinx import *
from audio.audiosystem import AudioSystem

VOCABULARY_DIR = 'en-us-rpi'
DATA_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), VOCABULARY_DIR)
DICTIONARY = 'cmudict-en-us.dict'
LANGUAGE_MODEL = 'en-us.lm.bin'
ACOUSTIC_MODEL = 'cmusphinx-en-us-ptm-5.2'

MODEL_PATH = os.path.join(DATA_DIR, ACOUSTIC_MODEL)
LANGUAGE_PATH = os.path.join(DATA_DIR, LANGUAGE_MODEL)
DICTIONARY_PATH = os.path.join(DATA_DIR, DICTIONARY)
POCKET_SPHINX_BIT_RATE = 16000 # rate matches the sphinx models

class SpeechToText(object):
    def getText(self):
        raise NotImplementedError("Subclasses must implement this method")
    def shutdown(self):
        raise NotImplementedError("Subclasses must implement this method")

class DefaultSpeechToText(SpeechToText):
    # A Wrapper that implements the SpeechToText interface
    def __init__(self):
        self.__stt = PocketSphinxListener()

    def getText(self):
        return self.__stt.getText()

    def shutdown(self):
        self.__stt.shutdown()

class PocketSphinxListener(object):
    # Adapted from https://github.com/slowrunner/Pi3RoadTest/blob/master/recoMic/pocket_sphinx_listen.py
    # Appears to have been patterned after https://github.com/cmusphinx/pocketsphinx/blob/master/swig/python/test/continuous_test.py
    # Configuration Documentation: https://sourceforge.net/p/cmusphinx/code/HEAD/tree/trunk/pocketsphinx/doc/pocketsphinx_batch.1#l414

    def __init__(self, acousticModel=MODEL_PATH, languageModel=LANGUAGE_PATH, dictionary=DICTIONARY_PATH):
        import logging
        self.__logger = logging.getLogger(__name__)
        self.__audio = AudioSystem()
        self.__inputSampleRate = POCKET_SPHINX_BIT_RATE
        config = Decoder.default_config()
        config.set_string('-hmm', acousticModel)
        config.set_string('-lm', languageModel)
        config.set_string('-dict', dictionary)
        config.set_boolean('-remove_silence', True)
        #config.set_int('-samprate', self.__inputSampleRate) does not work on Mac
        config.set_int('-vad_prespeech', 15)
        config.set_int('-vad_startspeech', 20)
        config.set_int('-vad_postspeech', 15) # Fixed long delay from speech to silence
        config.set_string('-logfn', '/dev/null')
        config.set_float("-vad_threshold", 2.0) # voice activation detection log ratio
        #config.set_boolean("-allphone_ci", True)

        self.__decoder = Decoder(config)

    def shutdown(self):
        self.__audio.shutdown()

    def getText(self):
        try:
            self.__audio.startRecording(bitrate=self.__inputSampleRate)
            utteranceStarted = False
            self.__decoder.start_utt()
            # We want this to loop for as long as it takes to get the full sentence from the user. We only exit with a
            # return statement when we have our best guess of what the person said.
            while True:
                # This takes a small sound bite from the microphone to process.
                soundBite = self.__audio.nextRecordedSoundBite()
                if soundBite is not None:
                    self.__decoder.process_raw(soundBite, False, False)
                    inSpeech = self.__decoder.get_in_speech()
                    # The following checks for the transition from silence to speech.
                    if inSpeech and not utteranceStarted:
                        utteranceStarted = True
                        #print('speech started')
                    # The following checks for the transition from speech to silence.
                    # This is our cue to check what was said and do something useful with it.
                    if not inSpeech and utteranceStarted:
                        #print('speech ended')
                        # We tell PocketSphinx that the user is finished saying what they wanted
                        # to say, and that it should makes it's best guess as to what thay was.
                        self.__decoder.end_utt()
                        # The following will get a hypothesis object with, amongst other things,
                        # the string of words that PocketSphinx thinks the user said.
                        hypothesis = self.__decoder.hyp()
                        if hypothesis is not None:
                            bestGuess = hypothesis.hypstr
                            self.__audio.stopRecording()
                            return bestGuess

        except Exception as e:
            self.__logger.error('Speech to text failed!', exc_info=True)
            return ""

if __name__ == "__main__":
    import sys, traceback
    #print("model path: {0}".format(MODEL_PATH))
    #print("language path: {0}".format(LANGUAGE_PATH))
    #print("dictionary path: {0}".format(DICTIONARY_PATH))
    pocketSphinxListener = PocketSphinxListener()
    while True:
        try:
            print("Say something ...")
            text = pocketSphinxListener.getText()
            print("I think you said: {0}".format(text))
        except (KeyboardInterrupt, SystemExit):
            print 'Goodbye.'
            sys.exit()
        except Exception as e:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            traceback.print_exception(exc_type, exc_value, exc_traceback,
                                      limit=4,
                                      file=sys.stdout)
            sys.exit()
