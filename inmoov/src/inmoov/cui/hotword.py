#!/usr/bin/env python
# Copyright (c) 2017 Mike Jacobs
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
from snowboy import snowboydecoder
import os, sys, traceback, signal

HOTWORD_MODEL_FILE_NAME = 'snowboy.pmdl'
HOTWORD_MODEL_DIR = os.path.dirname(os.path.realpath(__file__))
HOTWORD_MODEL_FULL_FILE_NAME =  os.path.join(HOTWORD_MODEL_DIR, HOTWORD_MODEL_FILE_NAME)

class HotwordListener(object):
    def onWakeup(self, aDetector):
        raise NotImplementedError( "Your listener should have implemented this")

class NullListener(HotwordListener):
    def onWakeup(self, aDetector):
        print("hotword listener worked")

class HotwordDetector(object):

    def __init__(self, model=HOTWORD_MODEL_FULL_FILE_NAME, listener=None):
        self.__detector = snowboydecoder.HotwordDetector(model)
        if(listener is None):
            self.__listener = NullListener()
        else:
            self.__listener = listener

        self.__interrupted = False

    def setInterrupted(self, aBoolean):
        self.__interrupted = aBoolean
        if aBoolean: self.__detector.terminate()

    def isInterrupted(self):
        return self.__interrupted

    def startListening(self):
        #print("*** HotwordDetector.startListening")
        self.__detector.start(detected_callback=self.__wakeup,
               interrupt_check=self.isInterrupted,
               sleep_time=0.03)

    def __wakeup(self):
        #print("*** HotwordDetector.__wakeup")
        self.__listener.onWakeup(self)

    def shutdown(self):
        self.setInterrupted(True)


if __name__ == "__main__":

    try:
        #print('*** HotwordDetector.__main__')
        def signal_handler(signal, frame):
            global detector
            detector.setInterrupted(True)

        detector = HotwordDetector()
        print("Say 'snow boy' to test the hotword detection")
        signal.signal(signal.SIGINT, signal_handler)
        detector.startListening()

    except:
        print("Exception occurred")
        detector.shutdown()
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exception(exc_type, exc_value, exc_traceback,
                              limit=5, file=sys.stdout)
