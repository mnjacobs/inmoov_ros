#! /usr/bin/env python

# Copyright (c) 2017 Mike Jacobs
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

from none_handler import NoneHandler

class CommandProcessor(object):

    def __init__(self):
        self.__handlers = {}
        self.__defaultHandler = NoneHandler()

    def handle(self, action, parameters):
        handler =  self.__handlers.get(action, self.__defaultHandler)
        return handler._handle(action, parameters)

    def register(self, aCommandHandler, default = False):
        key = aCommandHandler._getKey()
        self.__handlers[key] = aCommandHandler
        if default:
            self.__defaultHandler = aCommandHandler


try:
    processor = CommandProcessor()
except:
    import sys, traceback
    rospy.logwarn("Command Processor FAILED")
    exc_type, exc_value, exc_traceback = sys.exc_info()
    traceback.print_exception(exc_type, exc_value, exc_traceback, limit=5, file=sys.stdout)
