#! /usr/bin/env python

# Copyright (c) 2017 Mike Jacobs
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

from tts import DefaultTextToSpeech
from nlp import APIAINaturalLanguageProcessor
from audio.audiosystem import AudioSystem
from stt import DefaultSpeechToText
from command_processor import CommandProcessor
from change_voice_handler import ChangeVoiceHandler
from hotword import HotwordDetector, HotwordListener
from conversation import Conversation

import logging.config
import yaml
import os
CONFIG_DIR = os.path.dirname(os.path.realpath(__file__))
CONFIG_FILE_NAME = 'logging.yaml'
FULL_FILE_NAME = os.path.join(CONFIG_DIR, CONFIG_FILE_NAME)

if os.path.exists(FULL_FILE_NAME):
    with open(FULL_FILE_NAME, 'rt') as f:
        config = yaml.safe_load(f.read())
    logging.config.dictConfig(config)
else:
    logging.basicConfig(level=logging.INFO)


try:
    class MyHotwordListener(HotwordListener):
        def __init__(self, aConversation):
            self.__conversation = aConversation

        def onWakeup(self, aDetector):
            #  aDetector.shutdown() # stop the hotword detector during the conversation
            self.__conversation.start()

    nlp = APIAINaturalLanguageProcessor()
    tts = DefaultTextToSpeech()
    audio = AudioSystem()
    stt = DefaultSpeechToText()
    cmd = CommandProcessor()
    conversation = Conversation(stt, nlp, tts, audio, cmd)
    changeVoice = ChangeVoiceHandler(tts)
    conversation.register(changeVoice)
    print("Starting hotword detector")
    detector = HotwordDetector(listener=MyHotwordListener(conversation))
    print("Say 'snow boy' to start a conversation")
    detector.startListening()
except Exception as e:
    import sys
    import traceback
    exc_type, exc_value, exc_traceback = sys.exc_info()
    traceback.print_exception(exc_type, exc_value, exc_traceback, limit=8, file=sys.stdout)
