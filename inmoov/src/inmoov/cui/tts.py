#! /usr/bin/env python

# Copyright (c) 2016-2017 Mike Jacobs
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

import os, sys, traceback
from boto3 import Session
from botocore.exceptions import BotoCoreError, ClientError
from contextlib import closing
import hashlib, json
from time import sleep
import datetime, random, Queue, threading

from audio.audiosystem import AudioSystem

CACHE_LOCATION = "~/voices_cache/" # will be in the user's home dir

OUTPUT_FORMAT = "mp3" # must be one of: mp3 | ogg_vorbis | pcm
STREAM_FILETYPE = ".mp3"
OUTPUT_FILETYPE = ".wav"
VOICE_SAMPLE_RATE = '22050'
VOICE_PLAYBACK_RATE = 22050

class TextToSpeech(object):
    def say(self, text, synchronous=False):
        raise NotImplementedError("Subclasses must implement this method")
    def setVoiceGender(self, aGender):
        raise NotImplementedError("Subclasses must implement this method")
    def setVoiceCountry(self, aCountry):
        raise NotImplementedError("Subclasses must implement this method")
    def waitUntilDone(self, timeout=7.0):
        raise NotImplementedError("Subclasses must implement this method")
    def shutdown(self):
        raise NotImplementedError("Subclasses must implement this method")

class DefaultTextToSpeech(TextToSpeech):
    # A Wrapper that implements the TextToSpeech interface
    def __init__(self):
        self.__tts = PollyVoiceSynthesizer()

    def say(self, text, synchronous=True):
        self.__tts.say(text, synchronous=synchronous)

    def setVoiceGender(self, aGender):
        self.__tts.setVoiceGender(aGender)

    def setVoiceCountry(self, aCountry):
        self.__tts.setVoiceCountry(aCountry)

    def waitUntilDone(self, timeout=7.0):
        self.__tts.waitUntilDone(timeout)

    def shutdown(self):
        self.__tts.shutdown()

class VoiceSynthesizer(object):
    def __init__(self, volume=40):
        self._volume = volume

    def getVolume(self):
        return self._volume

    def setVolume(self, aVolume):
        self._volume = aVolume

    def say(self, text, synchronous=True):
        self._synthesize(text, synchronous=synchronous)

    def shutdown(self):
        raise NotImplementedError("Subclasses must implement this method")

    def waitUntilDone(self, timeout=7.0):
        raise NotImplementedError("Subclasses must implement this method")

    def _synthesize(self, text, synchronous):
        raise NotImplementedError("Subclasses must implement this method")


SALLI =  {'gender': "female", 'country': "american", 'name': "Salli"}
NICOLE = {'gender': "female", 'country': "australian", 'name': "Nicole"}
AMY = {'gender': "female", 'country': "british", 'name': "Amy"}
JOEY = {'gender': "male", 'country': "american", 'name': "Joey"}
RUSSEL = {'gender': "male", 'country': "australian", 'name': "Russell"}
BRIAN = {'gender': "male", 'country': "british", 'name': "Brian"}

VOICES = [SALLI, NICOLE, AMY, JOEY, RUSSEL, BRIAN]

class PollyVoiceSynthesizer(VoiceSynthesizer):
    def __init__(self, volume=40, voice = BRIAN):
        super(PollyVoiceSynthesizer,self).__init__(volume)
        self.__queue = Queue.Queue(0)
        session = Session(profile_name="default")
        self.__polly = session.client("polly")
        self.__setUpPlayThread()
        self.__audioPlayer = AudioSystem(volume=volume)
        self.__voice = voice

    def shutdown(self):
        self.__audioPlayer.shutdown()

    def setVoiceGender(self, aGender):
        country = self.getVoiceCountry()
        voice = self.findVoice(country, aGender)
        if voice is not None:
            self.setVoice(voice)
        else:
            print("Cannot set voice to {0} {1}".format(country, aGender))

    def setVoiceCountry(self, aCountry):
        gender = self.getVoiceGender()
        voice = self.findVoice(aCountry, gender)
        if voice is not None:
            self.setVoice(voice)
        else:
            print("Cannot set voice to {0} {1}".format(aCountry, gender))

    def getVoiceCountry(self):
        return self.__voice['country']

    def getVoiceGender(self):
        return self.__voice['gender']

    def findVoice(self, aCountry, aGender):
        subset = [voice for voice in VOICES if voice['country'] == aCountry]
        answer = [voice for voice in subset if voice['gender'] == aGender]
        if(len(answer) == 1):
            return answer[0]
        return None

    def setVoice(self, aVoice):
        self.__voice = aVoice

    def __setUpPlayThread(self):
        self.__playThread = threading.Thread(target=self.__playFromQueue, name="Voice player")
        self.__playThread.setDaemon(True)
        self.__playThread.start()

    def __playFromQueue(self):
        queue = self.__queue
        while True:
            entry = queue.get(block=True, timeout=None)
            soundFileName = entry['soundFileName']
            self.__playWave(soundFileName)
            queue.task_done()
            sleep(0) # equivalent to yielding the thread

    def __playWave(self, soundFileName):
        self.__audioPlayer.playWave(soundFileName)

    def __textToMessageDigest(self, text):
        # Create a unique id based on the voice id and text
        message = hashlib.md5()
        message.update(text)
        message.update(self.__voice['name'])
        return message.hexdigest()

    def _synthesize(self, text, synchronous):
        # Implementation specific synthesis with caching

        try:
            fullCacheLocation = os.path.expanduser(CACHE_LOCATION)
            cacheExists = os.path.exists(fullCacheLocation)
            if cacheExists is False:
                #print("Creating cache location of {0}".format(fullCacheLocation))
                os.makedirs(fullCacheLocation)

            fileName = self.__textToMessageDigest(text)
            soundFileName = os.path.join(fullCacheLocation, fileName + OUTPUT_FILETYPE)
            fileExists = os.path.exists(soundFileName)

            sound = None
            if fileExists is not True:
                # get stream data from AWS
                # print("using AWS")
                data = self.__getStreamFromAWS(text)
                # Save the audio file and the meta data
                streamFileName = os.path.join(fullCacheLocation, fileName + STREAM_FILETYPE)
                with open(streamFileName, "wb") as file:
                    file.write(data)
                self.__covertMP3toWAV(streamFileName, soundFileName)

            #print("{0} Queuing: {1} thread: {2}".format(datetime.datetime.now(), text, threading.current_thread().name))
            if synchronous:
                self.__playWave(soundFileName)
            else:
                self.__queue.put({'text' : text, 'soundFileName': soundFileName}, block=False)
            # request for sound is now queued and will be picked up by the playback thread

        except:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            traceback.print_exception(exc_type, exc_value, exc_traceback,
                              limit=5, file=sys.stdout)


    def waitUntilDone(self, timeout=7.0):
        self.__queue.join()
        #self.__playThread.join(timeout)

    def __covertMP3toWAV(self, aStreamFileName, aSoundFileName):
        import ffmpy, subprocess
        ff = ffmpy.FFmpeg(global_options='-loglevel error',
            inputs={aStreamFileName: None}, outputs={aSoundFileName: None})
        # print('*** ' + ff.cmd)
        ff.run()

    def __getStreamFromAWS(self, text):
        # Request speech synthesis
        data = None
        # Add slight pause to get around apparent issues with bluetooth audio
        ssml = "<speak><break time='100ms'/>"+text+"<break time='100ms'/></speak>"
        response = self.__polly.synthesize_speech(Text=ssml,
                            OutputFormat=OUTPUT_FORMAT, SampleRate=VOICE_SAMPLE_RATE,
                            VoiceId=self.__voice['name'], TextType='ssml')
        # Access the audio stream from the response
        if "AudioStream" in response:
            # Note: Closing the stream is important as the service throttles on the
            # number of parallel connections. Here we are using contextlib.closing to
            # ensure the close method of the stream object will be called automatically
            # at the end of the with statement's scope.
            with closing(response["AudioStream"]) as stream:
                data = stream.read()

        return data

if __name__ == "__main__":
    import sys, traceback
    import datetime
    # Test code
    debugging = False
    try:
        synthesizer = PollyVoiceSynthesizer()
        # Test text to speech for each voice
        statement = "My name is {0} and I am a {1} {2}"
        for option in VOICES:
            msg = statement.format(option['name'], option['country'], option['gender'])
            synthesizer.setVoice(option)
            sleep(0.4)
            synthesizer.say(msg, True)
        synthesizer.shutdown()
    except:
        print "exception occurred!"
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exception(exc_type, exc_value, exc_traceback,
                              limit=5, file=sys.stdout)

    print "done"
