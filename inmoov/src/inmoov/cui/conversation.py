#! /usr/bin/env python

# Copyright (c) 2017 Mike Jacobs
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
import time
from command_handler import CommandHandler
from audio.audiosystem import AudioSystem
from command_processor import CommandProcessor


class StopConversationHandler(CommandHandler):
    def __init__(self, aConversation):
        self.__conversation = aConversation

    def _getKey(self):
        return 'StopConversation'

    def _handle(self, action, parameters):
        self.__conversation.stop()
        return None


class InputUnknownHandler(CommandHandler):
    def _getKey(self):
        return 'input.unknown'

    def _handle(self, action, parameters):
        return None


class TranscriptListener(object):
    # Called each time there is an input or outputs
    def heard(self, text):  # the stt result
        raise NotImplementedError("Subclasses must implement this method")

    def said(self, text):  # the machine response
        raise NotImplementedError("Subclasses must implement this method")


class DefaultTranscriptListener(TranscriptListener):
    # Called each time there is an input or outputs
    def heard(self, text):  # the stt result
        print("You: {0}".format(text))

    def said(self, text):  # the machine response
        print("Me: {0}".format(text))


class CycleListener(object):
    # Called each time there is a back and forth
    def startCycle(self):  # the start of the cycle
        raise NotImplementedError("Subclasses must implement this method")

    def endCycle(self):  # the end of the cycle
        raise NotImplementedError("Subclasses must implement this method")


class DefaultCycleListener(CycleListener):
    # Called each time there is a back and forth between user and agent
    def startCycle(self):  # the start of the cycle
        print("Listening...")

    def endCycle(self):  # the end of the cycle
        pass


class Conversation(object):
    def __init__(self, stt, nlp, tts, audio=AudioSystem(), commandProcessor=CommandProcessor(), transcriptListener=DefaultTranscriptListener(), cycleListener=DefaultCycleListener()):
        """
        sst - implemenation of SpeechToText
        nlp - implemenation of NaturalLanguageProcessor
        tts - implemenation of TextToSpeech
        audio - AudioSystem instance
        commandProcessor - CommandProcessor instance
        """
        self.__audio = audio
        self.__tts = tts
        self.__stt = stt
        self.__nlp = nlp
        self.__commandProcessor = commandProcessor
        commandProcessor.register(StopConversationHandler(self))
        commandProcessor.register(InputUnknownHandler())
        self.__transcript = transcriptListener
        self.__cycleListener = cycleListener

    def stop(self):
        self.__inConversation = False

    def say(self, text):
        self.__tts.say(text)

    def register(self, aCommandHandler):
            self.__commandProcessor.register(aCommandHandler)

    def __heard(self, text):
        if(self.__transcript is not None):
            self.__transcript.heard(text)

    def __said(self, text):
        if(self.__transcript is not None):
            self.__transcript.said(text)

    def __startCycle(self):
        if(self.__cycleListener is not None):
            self.__cycleListener.startCycle()

    def __endCycle(self):
        if(self.__cycleListener is not None):
            self.__cycleListener.endCycle()

    def start(self):
        self.__audio.playOpeningSound()
        self.__inConversation = True
        while(self.__inConversation is True):
            self.__startCycle()
            text = self.__stt.getText()
            if(text is not None and len(text) > 0):
                self.__heard(text)
                nlpResults = self.__nlp.process(text)
                action = nlpResults.action
                parameters = nlpResults.parameters
                message = self.__commandProcessor.handle(action, parameters)
                speech = nlpResults.speech
                if speech is not None and len(speech) > 0:
                    self.__said(speech)
                    self.say(speech)
                if message is not None and len(message) > 0:
                    self.__said(message)
                    self.say(message)
            # time.sleep(0.1)
            self.__endCycle()

        self.__audio.playClosingSound()

    def shutdown(self):
        self.__audio.terminate()


if __name__ == "__main__":
    from tts import DefaultTextToSpeech
    from nlp import APIAINaturalLanguageProcessor
    from stt import DefaultSpeechToText
    from change_voice_handler import ChangeVoiceHandler
    try:
        nlp = APIAINaturalLanguageProcessor()
        tts = DefaultTextToSpeech()
        audio = AudioSystem()
        stt = DefaultSpeechToText()
        cmd = CommandProcessor()
        conversation = Conversation(stt, nlp, tts, audio, cmd)
        changeVoice = ChangeVoiceHandler(tts)
        conversation.register(changeVoice)
        print("Starting conversation")
        conversation.start()
    except Exception as e:
        import sys
        import traceback
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exception(exc_type, exc_value, exc_traceback, limit=8, file=sys.stdout)
