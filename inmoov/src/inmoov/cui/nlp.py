#! /usr/bin/env python

# Copyright (c) 2017 Mike Jacobs
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
import apiai
import json

CONVERSATIONAL_UI_AGENT_TOKEN = '3aa9ddadff1d464fb51f1b45b06efbbd' #unique to your api.ai agent

class NLPHandler(object):
    def _getKey(self):
        # For API.AI, this is the action associated with the Intent
        raise NotImplementedError("Subclasses must implement this method")

    def _handle(self, contexts, action, parameters, incomplete):
        raise NotImplementedError("Subclasses must implement this method")

class ChangeVoiceHandler(object):
    def __init__(self, tts):
        self.__tts = tts

    def _getKey(self):
        return 'ChangeVoice' # must match the action from the api.ai agent/intent

    def _handle(self, contexts, action, parameters, incomplete):
        aCountry = parameters.get('country')
        aGender = parameters.get('gender')
        print("Changing voice to country: {0} gender: {1}".format(aCountry, aGender))
        if aCountry is not None and (len(aCountry) > 0):
            self.__tts.setVoiceCountry(aCountry)
        if aGender is not None and (len(aGender) > 0):
            self.__tts.setVoiceGender(aGender)

class NLPResults(object):
    def __init__(self, contexts, action, parameters, incomplete, speech):
        self.contexts = contexts
        self.action = action
        self.parameters = parameters
        self.incomplete = incomplete
        self.speech = speech

class NaturalLanguageProcessor(object):

    def process(self, text):
        # Must return an instance of NLPResults
        raise NotImplementedError("Subclasses must implement this method")

    def shutdown(self):
        raise NotImplementedError("Subclasses must implement this method")

class APIAINaturalLanguageProcessor(NaturalLanguageProcessor):
    def __init__(self, token=CONVERSATIONAL_UI_AGENT_TOKEN):
        self.__api = apiai.ApiAI(token)

    def process(self, text):
        contexts = []
        action = "*nothing*"
        parameters = None
        incomplete = False
        speech = "Something went terribly wrong during NLP processing"

        request = self.__api.text_request()
        request.lang = 'en'  # optional, default value equal 'en'
        request.query = text
        response = request.getresponse().read()
        #print(response)
        dictionary = json.loads(response)
        status = dictionary['status']
        errorType = status['errorType']

        if errorType == 'success': # successful call/return
            result = dictionary.get('result')
            if result is not None:
                action = result.get('action', action)
                incomplete = result.get('actionIncomplete', incomplete) # if slot filling is required
                parameters = result.get('parameters', parameters) # parameters needed by the action
                contexts = result.get('contexts', contexts)
                fulfillment = result.get('fulfillment')
                if fulfillment is not None:
                    speech = fulfillment.get('speech', speech)

        return NLPResults(contexts, action, parameters, incomplete, speech)

    def shutdown(self):
        pass

if __name__ == "__main__":

    def callNLP(nlp, inputText):
        print("Speaker: {0}".format(inputText))
        response = nlp.process(inputText)
        action = response.action
        speech = response.speech
        print('Chat Bot: {0} action: {1}'.format(speech, action) )

    try:
        nlp = APIAINaturalLanguageProcessor()
        callNLP(nlp, "let's change your voice to a female brit")
        callNLP(nlp, "how are you today?") # Small Talk intent
        callNLP(nlp, "update your sound to an american man")
        callNLP(nlp, "are you married?")  # Small Talk intent
        callNLP(nlp, "change how you sound")
        callNLP(nlp, "female")
        callNLP(nlp, "australian")
    except:
        import sys, traceback
        print("APIAINaturalLanguageProcessor FAILED")
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exception(exc_type, exc_value, exc_traceback, limit=8, file=sys.stdout)
