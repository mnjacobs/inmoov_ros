#!/usr/bin/python

# Copyright (c) 2016 Mike Jacobs
# 
# Permission is hereby granted, free of charge, to any person obtaining 
# a copy of this software and associated documentation files 
# (the "Software"), to deal in the Software without restriction, 
# including without limitation the rights to use, copy, modify, merge, 
# publish, distribute, sublicense, and/or sell copies of the Software, 
# and to permit persons to whom the Software is furnished to do so, 
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 

""" 
This module combines support for Servos of varying classes as well as
the Adafruit 16-Channel 12-bit PWM/Servo Driver - I2C interface - PCA9685
"""
from Adafruit_PWM_Servo_Driver import PWM
import time
import datetime

class ServoController(object):
    """ Adafruit 16-Channel 12-bit PWM/Servo Driver - I2C interface - PCA9685 
        https://learn.adafruit.com/adafruit-16-channel-servo-driver-with-raspberry-pi/library-reference
    """
    def __init__(self, i2cAddress=0x40, frequency=50, debug=False):
        """ i2cAddress - the I2C bus address for this controller between 0x40 and 0x77
            frequency - The frequency of the PWM duty cycle between 40 and 1000 Hz 
        """ 
        self.pwm = PWM(i2cAddress, debug)
        self.pwm.setPWMFreq(frequency)
        self.frequency = frequency
        self.debug = debug
        
    def _millisecondsToTicks(self, milliseconds):
        """ Private: convert PWM pulse width duration (in milliseconds) to 
            a resolution and frequency specific number of ticks to send to
            the controller.
        """
        return int(round((4096.0*self.frequency*milliseconds)/1000.0))
        
    def __lerp(self, v0, v1, t) :
        """ Private: perform precise linear interpolation of t between v0 and v1 """
        lerp = (1.0-t)*v0 + t*v1
        return lerp
    
    def _setPosition(self, servo, degrees):
        # Linearly interpolate between minMilliseconds and maxMilliseconds
        angleRatio = (degrees - servo.minAngle)/servo.angleRange
        millis = self.__lerp(servo.minMilliseconds, servo.maxMilliseconds, angleRatio)
        if(self.debug) :
            print("[{4}] {3} angle = {0}, angleRatio = {1}, millis = {2}".format(degrees, angleRatio, millis, servo.getName(), datetime.datetime.now()))
        self._setPulseWidth(servo, millis)
    
    def _setPulseWidth(self, servo, millis):
        ticks = self._millisecondsToTicks(millis)
        if(self.debug) :
            print("[{3}] {2} millis = {3}, ticks = {0}".format(ticks, millis, servo.getName(), datetime.datetime.now()))
        self.pwm.setPWM(servo.channel, 0, ticks)
        time.sleep(0.05) # allow time between I2C writes to allow time for the ACK clock pulse on SCL
        
        
class Servo(object):
    """ Abstract super class for servos (not enforced) """
    def __init__(self, controller, channel, angleRange, minMilliseconds, midMilliseconds, maxMilliseconds, debug=False):
        """ controller - an instance of ServoController
            channel - the controller channel (0-15) that the servo is attached to
            angleRange - the sweep range specified by the manufacturer
            minMilliseconds - the pulse width specified by the manufacturer to set the servo to the minimum position
            midMilliseconds - the pulse width specified by the manufacturer to set the servo to the neutral position
            maxMilliseconds - the pulse width specified by the manufacturer to set the servo to the maximum position
        """
        self.controller = controller
        self.__name = None
        self.channel = channel
        self.minMilliseconds = minMilliseconds
        self.maxMilliseconds = maxMilliseconds
        self.debug = debug
        self.angleRange = angleRange
        self.currentAngle = 0
        halfAngleRange = angleRange/2.0
        if(halfAngleRange > 45.0) : 
            # Assumes servos act symmetrically
            self.minAngle = 90.0 - halfAngleRange
            self.maxAngle = 90.0 + halfAngleRange
        else :
            self.minAngle = 45.0 - halfAngleRange
            self.maxAngle = 45.0 + halfAngleRange
        # Default constraint is the manufacturer's sweep spec
        self.constrainAngles(self.minAngle, self.maxAngle)
            
    
    def constrainAngles(self, minConstraint, maxConstraint):
        """ Constrain the sweep range between min and max constraints """
        if(minConstraint >= self.minAngle): self.minConstraint = minConstraint 
        else : self.minConstraint = self.minAngle
        if(maxConstraint <= self.maxAngle): self.maxConstraint = maxConstraint
        else : self.maxConstraint = self.maxAngle
        
        if(self.debug):
            print('DEBUG: {0} minConstraint={1} maxConstraint={2}'.format(self.getName(), minConstraint, maxConstraint))
            print('DEBUG: {0} minAngle={1} maxAngle={2}'.format(self.getName(), self.minAngle, self.maxAngle))
            print('DEBUG: {0} minConstraint={1} maxConstraint={2}'.format(self.getName(), self.minConstraint, self.maxConstraint))
    
    def getAngle(self):
        return self.currentAngle
        
    def setAngle(self, degrees):
        # Check requested position against constraints
        adjusted = self.getConstrainAdjustedValue(degrees)
        if(self.debug):
            print("DEBUG: {0} requested={1} adjusted={1}".format(self.getName(), degrees, adjusted))
        self.controller._setPosition(self, adjusted)
        self.currentAngle = adjusted
    
    def setPulseWidth(self, millis, constrain=True):
        adjusted = millis
        if(constrain):
            if(millis < self.minMilliseconds) : adjusted = self.minMilliseconds
            elif(millis > self.maxMilliseconds) : adjusted = self.maxMilliseconds
        self.controller._setPulseWidth(self, adjusted)
    
    def getConstrainAdjustedValue(self, degrees):
        adjusted = degrees
        if(degrees < self.minConstraint) : adjusted = self.minConstraint
        elif(degrees > self.maxConstraint) : adjusted = self.maxConstraint  
        return adjusted
         
    def setName(self, aName):
        self.__name = aName 
    
    def getName(self):
        if(self.__name == None):
            self.__name = "Unnamed @ ch: {0} ".format(self.channel)
        return self.__name

class PowerHD1501MGServo(Servo):
    def __init__(self, controller, channel, debug=False):
        # Use the values from the product datasheet
        #super(PowerHD1501MGServo,self).__init__(controller, channel, 180, 0.8, 1.5, 2.2, debug)
        # The data sheet values only moved the servo about 90 degrees.  Through experimentation,
        # these values were found to provide maximum movement
        super(PowerHD1501MGServo,self).__init__(controller, channel, 180, 0.5, 1.5, 2.6, debug)

if __name__ == "__main__":
    import sys, traceback
    # Test code  
    print "******starting" 
    debugging = False
    try: 
        controller = ServoController(0x40, 50, debugging)
        #servo = PowerHD1501MGServo(controller, 5, debugging)
        #servo.setPulseWidth(0.5, constrain=False)
        #time.sleep(2.0)
        #servo.setPulseWidth(1.5, constrain=False)
        #time.sleep(2.5)
        #servo.setPulseWidth(2.6, constrain=False)
        #time.sleep(2.5)
        #servo.setPulseWidth(0.5, constrain=False)
        servoList = [PowerHD1501MGServo(controller, port, debugging) for port in range(6)]
 
        for servo in servoList:
            servo.setAngle(0)
        time.sleep(2)
        for servo in servoList:
            servo.setAngle(180)
        time.sleep(2)
        for servo in servoList:
            servo.setAngle(0)
          
       
    except:
        print "exception occurred!"
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exception(exc_type, exc_value, exc_traceback,
                              limit=5, file=sys.stdout)
    
    print "done"
    


