#! /usr/bin/env python

# Copyright (c) 2017 Mike Jacobs
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

import rospy
from inmoov.srv import NLP, TTS
from inmoov.srv import SpeechToText
from inmoov.msg import StartConversation, Status
from tts.messages import MESSAGES
from cui.conversation import Conversation
import Queue

NODE_NAME = 'Conversation'
STT_SERVICE_NAME = 'stt'
NLP_SERVICE_NAME = 'nlp'
START_CONVERSATION_TOPIC = 'start_conversation'
TTS_SERVICE_NAME = 'tts'

IMPROMPTU_STATUS_REPORTING_TOPIC = 'impromptu_status'
STATUS_QUEUE_SIZE = 2

from cui.stt import SpeechToText as CUISpeechToText
class STTService(CUISpeechToText):
    def __init__(self, aService):
        self.__service = aService
    def getText(self):
        # Return the spoken text
        response = self.__service(None)
        return response.text
    def shutdown(self):
        pass

from cui.nlp import NaturalLanguageProcessor
class NLPService(NaturalLanguageProcessor):
    def __init__(self, aService):
        self.__service = aService
    def process(self, text):
        # Returns NLPResults
        return self.__service(text)
    def shutdown(self):
        pass

from cui.tts import TextToSpeech
class TTSService(TextToSpeech):
    def __init__(self, aService):
        self.__service = aService
    def say(self, text):
        self.__service(text)

    def setVoiceGender(self, aGender):
        raise NotImplementedError("Subclasses must implement this method")
    def setVoiceCountry(self, aCountry):
        raise NotImplementedError("Subclasses must implement this method")

class ConversationManager(object):
    def __init__(self):
        rospy.init_node(NODE_NAME)
        rospy.on_shutdown(self.__onShutdown)
        self.__statusSubscriber = rospy.Subscriber(IMPROMPTU_STATUS_REPORTING_TOPIC, Status, self.__onIncomingStatus, queue_size = STATUS_QUEUE_SIZE)
        self.__startSubscriber = rospy.Subscriber(START_CONVERSATION_TOPIC, StartConversation, self.__onStart, queue_size = 1)

        sttService = STTService(rospy.ServiceProxy(STT_SERVICE_NAME, SpeechToText, persistent=True))
        nlpService = NLPService(rospy.ServiceProxy(NLP_SERVICE_NAME, NLP, persistent=True))
        ttsService = TTSService(rospy.ServiceProxy(TTS_SERVICE_NAME, TTS, persistent=True))

        self.__conversation = Conversation(sttService, nlpService, ttsService)

        from command.network_handler import NetworkHandler
        from command.leapmotion_handler import LeapMotionHandler
        self.__conversation.register(NetworkHandler())
        self.__conversation.register(LeapMotionHandler())

        rospy.wait_for_service(TTS_SERVICE_NAME)
        rospy.wait_for_service(STT_SERVICE_NAME)
        rospy.wait_for_service(NLP_SERVICE_NAME)
        self.__conversation.say(MESSAGES['MY_NAME'])
        self.__conversation.say(MESSAGES['AWAITING_INSTRUCTIONS'])
        self.__statusQueue = Queue.Queue(0)

    def __onIncomingStatus(self, aStatus):
        # unsolicited status updates that could interrupt a conversation
        rospy.loginfo("ConversationManager__onIncomingStatus")
        if(self.__inConversation is True):
            self.__statusQueue.put(aStatus, block=False)
        else:
            self.__handleStatus(aStatus)

    def __handleQueuedStatus(self):
        queue = self.__statusQueue
        while queue.empty() is False:
            try:
                status = queue.get(block=False)
                self.__handleStatus(status)
                rospy.sleep(0)
            except Empty:
                break

    def __handleStatus(self, aStatus):
        component = aStatus.component
        severity = aStatus.severity
        message = aStatus.message
        #print("ConversationManager__handleStatus: " + message)
        self.__conversation.say(message)

    def __onStart(self, request):
        self.__conversation.start()

    def __onShutdown(self):
        self.__startSubscriber.unregister()
        self.__statusSubscriber.unregister()

try:
    manager = ConversationManager()
    rospy.spin()
except:
    rospy.logwarn("Conversation Management FAILED")
    import sys, traceback
    exc_type, exc_value, exc_traceback = sys.exc_info()
    traceback.print_exception(exc_type, exc_value, exc_traceback, limit=8, file=sys.stdout)
