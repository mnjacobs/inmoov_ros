#!/usr/bin/env python
# Copyright (c) 2017 Mike Jacobs
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import rospy
from inmoov.srv import NLP, NLPResponse
from inmoov.msg import KeyValuePair
from cui.nlp import APIAINaturalLanguageProcessor

NODE_NAME = 'NLP'
NLP_SERVICE_NAME = 'nlp'
INMOOV_AGENT_TOKEN = 'f671a4c903d8450bac4cfb56e358a0bc' #unique to your api.ai agent

class NLPNode(object):
    def __init__(self):
        rospy.init_node(NODE_NAME)
        rospy.on_shutdown(self.__onShutdown)
        self.__service = rospy.Service(NLP_SERVICE_NAME, NLP, self.__process)
        self.__nlp = APIAINaturalLanguageProcessor(INMOOV_AGENT_TOKEN)

    def __process(self, request):
        text = request.text
        # returns a NLPResults
        results = self.__nlp.process(text)
        # Convert into ROS compatible version
        action = results.action
        parameters = results.parameters # dictionary
        speech = results.speech
        keyValuePairs = []
        if parameters is not None:
            for k, v in parameters.iteritems():
                kvp = KeyValuePair(k, v)
        response = NLPResponse(action, keyValuePairs, speech)
        #print("nlp_node response is ....")
        #print(response)
        return response

    def __onShutdown(self):
        self.__service.shutdown()
        self.__nlp.shutdown()

try:
    node = NLPNode()
    rospy.spin()
except:
    rospy.logwarn("NLPNode FAILED")
    import sys, traceback
    exc_type, exc_value, exc_traceback = sys.exc_info()
    traceback.print_exception(exc_type, exc_value, exc_traceback, limit=5, file=sys.stdout)
