#!/usr/bin/env python
# Copyright (c) 2017 Mike Jacobs
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
from cui.stt import DefaultSpeechToText
import rospy
from inmoov.srv import SpeechToText

NODE_NAME = 'STT'
STT_SERVICE_NAME = 'stt'

class STTNode(object):
    def __init__(self):
        #print('*** SpeechToText.__init__')
        self.__stt = DefaultSpeechToText()
        rospy.init_node(NODE_NAME)
        rospy.on_shutdown(self.__onShutdown)
        self.__service = rospy.Service(STT_SERVICE_NAME, SpeechToText, self.__convert)

    def __convert(self, request):
        #print('*** SpeechToText.__convert')
        command = self.__stt.getText()
        #print(command)
        return command

    def __onShutdown(self):
        #print('*** SpeechToText.__onShutdown')
        self.__service.shutdown()
        self.__stt.shutdown()

try:
    converter = STTNode()
    rospy.spin()
except:
    rospy.logwarn("Speech to text FAILED")
    import sys, traceback
    exc_type, exc_value, exc_traceback = sys.exc_info()
    traceback.print_exception(exc_type, exc_value, exc_traceback, limit=5, file=sys.stdout)
