#!/usr/bin/env python
# Copyright (c) 2017 Mike Jacobs
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import sys, signal, os, traceback
import rospy
from inmoov.msg import StartConversation
from std_msgs.msg import String
from cui.hotword import HotwordDetector, HotwordListener
from inmoov.srv import SpeechToText

NODE_NAME = 'Hotword'
TOPIC = 'start_conversation'
PUBLISHER_QUEUE_SIZE = 1
HOTWORD_MODEL_FILE_NAME = 'jarvis.pmdl'
HOTWORD_MODEL_DIR = os.path.dirname(os.path.realpath(__file__))
HOTWORD_MODEL_FULL_FILE_NAME =  os.path.join(HOTWORD_MODEL_DIR, HOTWORD_MODEL_FILE_NAME)

class HotwordNode(HotwordListener):
    def __init__(self):
        rospy.init_node(NODE_NAME)
        rospy.on_shutdown(self.__onShutdown)

        self.__hotWordPublisher = rospy.Publisher(TOPIC, StartConversation, queue_size=PUBLISHER_QUEUE_SIZE)
        self.__detector = HotwordDetector(model=HOTWORD_MODEL_FULL_FILE_NAME, listener=self)
        rospy.sleep(3.0) # publishers don't start up right away

    def startListening(self):
        self.__detector.startListening()

    def onWakeup(self):
        event = StartConversation()
        self.__hotWordPublisher.publish(event)

    def __onShutdown(self):
        self.__detector.shutdown()
        self.__hotWordPublisher.unregister()

if __name__ == "__main__":

    try:
        #print('*** HotwordNode.__main__')
        node = HotwordNode()
        #rospy.sleep(3.0)
        node.startListening()
    except:
        rospy.logwarn("Hotword Node FAILED")
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exception(exc_type, exc_value, exc_traceback,
                              limit=5, file=sys.stdout)
