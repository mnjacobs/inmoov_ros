#! /usr/bin/env python

# Copyright (c) 2017 Mike Jacobs
# 
# Permission is hereby granted, free of charge, to any person obtaining 
# a copy of this software and associated documentation files 
# (the "Software"), to deal in the Software without restriction, 
# including without limitation the rights to use, copy, modify, merge, 
# publish, distribute, sublicense, and/or sell copies of the Software, 
# and to permit persons to whom the Software is furnished to do so, 
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
import rospy
from rosgraph_msgs.msg import Log

# Monitor rosout across multiple nodes and print on deployed node
# This is useful for monitoring code across all computers on the robot
NODE_NAME = 'LogMonitor'
SUBSCRIBED_TOPIC = '/rosout'
subscriber = None

def handleMessage(logEntry):
    msg = logEntry.msg
    print("LOG MONITOR: " + msg)
    
def onShutdown():
    global subscriber
    rospy.sleep(5.0) # try to hang around until other nodes shutdown
    if(subscriber is not None):
        subscriber.unregister()
    
rospy.init_node(NODE_NAME)
rospy.on_shutdown(onShutdown)
subscriber = rospy.Subscriber(SUBSCRIBED_TOPIC, Log, handleMessage, queue_size = 10)

try:
    print("*** LOG MONITOR STARTED ***")
    rospy.spin()
except:
    print("Log monitor FAILED")
    import sys, traceback
    exc_type, exc_value, exc_traceback = sys.exc_info()
    traceback.print_exception(exc_type, exc_value, exc_traceback, limit=5, file=sys.stdout)
