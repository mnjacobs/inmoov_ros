# Copyright (c) 2017 Mike Jacobs
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

# Adapted from https://realpython.com/blog/python/face-detection-in-python-using-a-webcam/
# and http://docs.opencv.org/master/d7/d8b/tutorial_py_face_detection.html
# Added cascade path assembly
# Adjusted for OpenCV 3.2.0
import cv2
import sys, os
openCVDataPath = '/home/pi/opencv-3.2.0/data/haarcascades/'
faceCascadeName = 'haarcascade_frontalface_default.xml'
eyeCascadeName = 'haarcascade_eye_tree_eyeglasses.xml'
faceCascadePath =  os.path.join(openCVDataPath, faceCascadeName)
eyeCascadePath =  os.path.join(openCVDataPath, eyeCascadeName)

# Check to make sure we can find the cascade
# cascadeExists = os.path.exists(faceCascadePath)
# print("Cascade {0} can be found: {1}".format(faceCascadePath, cascadeExists))
print("Press the 'q' key to quit")

faceCascade = cv2.CascadeClassifier(faceCascadePath)
eyeCascade = cv2.CascadeClassifier(eyeCascadePath)
VIDEO_WIDTH = 320.0
VIDEO_HEIGHT = 240.0
video_capture = cv2.VideoCapture(0) # assumes one camera attached
width = video_capture.get(cv2.CAP_PROP_FRAME_WIDTH)
height = video_capture.get(cv2.CAP_PROP_FRAME_HEIGHT)
print("{0} x {1}".format(width, height))
video_capture.set(cv2.CAP_PROP_FRAME_WIDTH, VIDEO_WIDTH)
video_capture.set(cv2.CAP_PROP_FRAME_HEIGHT, VIDEO_HEIGHT)
width = video_capture.get(cv2.CAP_PROP_FRAME_WIDTH)
height = video_capture.get(cv2.CAP_PROP_FRAME_HEIGHT)
print("{0} x {1}".format(width, height))
#video_capture.open(0)
frameCount = 0

import time
startTime = time.time()

while True:
    # Capture frame-by-frame
    success, frame = video_capture.read()
    if(success):
        frameCount = frameCount + 1
        if(frameCount % 50 == 0):
            now = time.time()
            frameRate = (frameCount*1.0)/(1.0 *(now - startTime))
            print("Frame rate: {0} FPS".format(frameRate))
        # convert frame to gray scale
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        # detect any faces
        faces = faceCascade.detectMultiScale(
            gray,
            scaleFactor=1.3,
            minNeighbors=5,
            minSize=(30, 30),
            flags=cv2.CASCADE_SCALE_IMAGE
        )

        # Draw a rectangle around the faces
        for (x, y, w, h) in faces:
            cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
            roi_gray = gray[y:y+h, x:x+w] # region of interest is the face
            roi_color = frame[y:y+h, x:x+w]
            # detect the eyes
            eyes = eyeCascade.detectMultiScale(roi_gray)
            for (ex,ey,ew,eh) in eyes:
                cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(255,0,0),2)

        # Display the resulting frame
        cv2.imshow('Face Detection Demo', frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything is done, release the capture
video_capture.release()
cv2.destroyAllWindows()
