#! /usr/bin/env python

# Copyright (c) 2017 Mike Jacobs
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

import rospy
from tts.messages import MESSAGES, MessageUtilities
from cui.command_handler import CommandHandler

URL_TO_CHECK_STATUS = 'www.google.com'

class NetworkHandler(CommandHandler):

    def _getKey(self):
        return 'GetNetworkStatus'

    def _handle(self, action, parameters):
        text = self.__getStatus()
        return text

    def __getStatus(self):
        text = MESSAGES['INTERNET_NOT_OK']
        if self.__isConnected():
            text = MessageUtilities.internetOK()
        return text

    def __isConnected(self):
        import socket
        try:
            # connect to the host -- tells us if the host is actually reachable
            socket.create_connection((URL_TO_CHECK_STATUS, 80))
            return True
        except OSError as e:
            rospy.logerror(e)
            pass
        return False
