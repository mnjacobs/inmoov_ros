#! /usr/bin/env python

# Copyright (c) 2017 Mike Jacobs
# 
# Permission is hereby granted, free of charge, to any person obtaining 
# a copy of this software and associated documentation files 
# (the "Software"), to deal in the Software without restriction, 
# including without limitation the rights to use, copy, modify, merge, 
# publish, distribute, sublicense, and/or sell copies of the Software, 
# and to permit persons to whom the Software is furnished to do so, 
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 

# Action is a list of Intents on api.ai agent
Action = {'UNKNOWN' : "input.unknown", 'GET_STATUS' : "get_status", 'STOP' : "stop"}

# Context is a list of Contexts on api.ai agent
Context = {'UNKNOWN' : "unknown", 'STATUS' : "status", 'CONVERSATION' : "conversation"}

# Component is an Entity on api.ai agent
Component = {'UNKNOWN': "unknown", 'ROBOT': "robot", 'NETWORK': "network", 
            'VR_CONTROLLER' : "leap motion controller", 'CONVERSATION' : "conversation"}
