#! /usr/bin/env python

# Copyright (c) 2017 Mike Jacobs
# 
# Permission is hereby granted, free of charge, to any person obtaining 
# a copy of this software and associated documentation files 
# (the "Software"), to deal in the Software without restriction, 
# including without limitation the rights to use, copy, modify, merge, 
# publish, distribute, sublicense, and/or sell copies of the Software, 
# and to permit persons to whom the Software is furnished to do so, 
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
import random

MESSAGES = {
'AWAITING_INSTRUCTIONS' : "Awaiting your instructions.",
'DEBUG'                 : "debug",
'DONE'                  : "done",
'ERROR'                 : "error!",
'EXCELLENT'             : "excellent!", 
'FATAL_ERROR'           : "fatal error!", 
'GOOD_DAY'              : "good day!", 
'GREETINGS'             : "Greetings!", 
'HELLO'                 : "Hello!", 
'HI'                    : "Hi!", 
'I_DONT_KNOW_THAT'      : "Sorry, but I don't know how to do that yet",
'INFO'                  : " ",
'INTERNET_OK'           : "The connection to the internet is working",
'INTERNET_OK2'          : "I can reach the internet just fine",
'INTERNET_OK3'          : "The network appears to be working",
'INTERNET_OK4'          : "I've confirmed that I'm connected to the network",
'INTERNET_NOT_OK'       : "The connection to the internet is not working",
'MONITORING_LOG'        : "I'm now monitoring the log messages",
'MY_NAME'               : "My name is Jarvis",
'NO'                    : "No", 
'OKAY'                  : "Okay", 
'RIGHTY_OH'             : "Righty oh",
'SORRY_BUT_NO'          : "Sorry, but no", 
'SHUTTING_DOWN'         : "Shutting down.", 
'STOPPED'               : "Stopped", 
'VERY_GOOD'             : "Very Good!", 
'VERY_GOOD_SIR'         : "Very Good Sir!",
'VOICE_STATUS_STARTED'  : "hello.  voice status is enabled",
'WARNING'               : "Warning",
'YES'                   : "Yes", 
'YES?'                  : "Yes?",
'YES_SIR'               : "Yes sir", 
'YES_SIR?'              : "Yes sir?"
}

WAKEUP = [MESSAGES['YES?']]
INTERNETOK=[MESSAGES['INTERNET_OK'], MESSAGES['INTERNET_OK2'], MESSAGES['INTERNET_OK3'], MESSAGES['INTERNET_OK4']]
END_CONVERSATION=[MESSAGES['VERY_GOOD'], MESSAGES['RIGHTY_OH'], MESSAGES['OKAY'], MESSAGES['DONE']]

class MessageUtilities(object):
    @staticmethod
    def wakeup():
        index = random.randint(0, len(WAKEUP)-1)
        return WAKEUP[index]
    @staticmethod
    def internetOK():
        index = random.randint(0, len(INTERNETOK)-1)
        return INTERNETOK[index]
    @staticmethod
    def endConversation():
        index = random.randint(0, len(END_CONVERSATION)-1)
        return END_CONVERSATION[index]
    
