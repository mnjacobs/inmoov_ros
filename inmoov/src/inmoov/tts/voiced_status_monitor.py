#! /usr/bin/env python

# Copyright (c) 2016-2017 Mike Jacobs
# 
# Permission is hereby granted, free of charge, to any person obtaining 
# a copy of this software and associated documentation files 
# (the "Software"), to deal in the Software without restriction, 
# including without limitation the rights to use, copy, modify, merge, 
# publish, distribute, sublicense, and/or sell copies of the Software, 
# and to permit persons to whom the Software is furnished to do so, 
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
import rospy
from rosgraph_msgs.msg import Log
from std_msgs.msg import String
from messages import MESSAGES
import socket

NODE_NAME = 'voice_status_monitor'
PUBLISHED_TOPIC = 'tts'
SUBSCRIBED_TOPIC = '/rosout'
PUBLISHER_QUEUE_SIZE = 10
subscriber = None

LEVELS = {1: MESSAGES['DEBUG'], 2 : MESSAGES['INFO'], 4 : MESSAGES['WARNING'], 8 : MESSAGES['ERROR'], 16 : MESSAGES['FATAL_ERROR']}


def handleMessage(logEntry):
    msg = LEVELS[logEntry.level] + " " + logEntry.msg
    global publisher
    publisher.publish(String(msg))
    
def onShutdown():
    global subscriber
    global publisher
    #rospy.loginfo(MESSAGES['SHUTTING_DOWN'])
    rospy.sleep(5.0) # try to hang around until other nodes shutdown
    if(publisher is not None):
        publisher.unregister()
    if(subscriber is not None):
        subscriber.unregister()
    
rospy.init_node(NODE_NAME)
rospy.on_shutdown(onShutdown)

publisher = rospy.Publisher(PUBLISHED_TOPIC, String, tcp_nodelay=True, latch=True, queue_size=PUBLISHER_QUEUE_SIZE)
subscriber = rospy.Subscriber(SUBSCRIBED_TOPIC, Log, handleMessage, queue_size = 10)

rospy.loginfo(MESSAGES['MONITORING_LOG'])


try:
    rospy.spin()
except:
    rospy.logwarn("Status monitor FAILED")
