#! /usr/bin/env python

# Copyright (c) 2017 Mike Jacobs
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
from messages import MESSAGES
from cui.tts import PollyVoiceSynthesizer, CACHE_LOCATION
import sys, traceback, os

try:
    fullCacheLocation = os.path.expanduser(CACHE_LOCATION)
    print("Clearing the cache at: {0}".format(fullCacheLocation))
    filesToRemove = [f for f in os.listdir(fullCacheLocation)]
    for f in filesToRemove:
        fullFileName = os.path.join(fullCacheLocation, f)
        os.remove(fullFileName)

    synthesizer = PollyVoiceSynthesizer()
    print("building voice cache...")
    for msg in MESSAGES.values():
        print(msg)
        synthesizer.say(msg, synchronous=True)

    synthesizer.waitUntilDone(3.0)
    print("voice cache build was successful")
except:
    print "exception occurred!"
    exc_type, exc_value, exc_traceback = sys.exc_info()
    traceback.print_exception(exc_type, exc_value, exc_traceback,
                          limit=5, file=sys.stdout)
