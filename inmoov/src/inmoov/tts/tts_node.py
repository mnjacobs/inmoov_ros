#! /usr/bin/env python

# Copyright (c) 2017 Mike Jacobs
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
import rospy
from cui.tts import DefaultTextToSpeech
from inmoov.srv import TTS, TTSResponse
from std_msgs.msg import String

NODE_NAME = 'text_to_speech'
TTS_TOPIC = 'tts'
TTS_SERVICE_NAME = 'tts'

class TTSNode(object):
    def __init__(self):
        rospy.init_node(NODE_NAME)
        rospy.on_shutdown(self.__onShutdown)
        self.__synthesizer = DefaultTextToSpeech()
        self.__subscriber = rospy.Subscriber(TTS_TOPIC, String, self.__handleMessage, queue_size = 10)
        self.__service = rospy.Service(TTS_SERVICE_NAME, TTS, self.__process)

    def __handleMessage(self, msg):
        # Subscribers use the asynchronous/queued approach
        self.__synthesizer.say(msg.data, False)

    def __process(self, request):
        # Service clients use the synchronous approach
        text = request.text
        start = rospy.get_rostime()
        self.__synthesizer.say(text, True)
        stop = rospy.get_rostime()
        #rospy.loginfo("TTS: {0} in {1} seconds".format(text, (stop-start).to_sec()))
        return TTSResponse()

    def __onShutdown(self):
        self.__synthesizer.waitUntilDone(8.0)
        self.__synthesizer.shutdown()
        self.__subscriber.unregister()
        self.__service.shutdown()

try:
    node = TTSNode()
    rospy.spin()
except:
    rospy.logwarn("Text to speech FAILED")
    import sys, traceback
    exc_type, exc_value, exc_traceback = sys.exc_info()
    traceback.print_exception(exc_type, exc_value, exc_traceback, limit=5, file=sys.stdout)
