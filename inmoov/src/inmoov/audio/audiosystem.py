#! /usr/bin/env python

# Copyright (c) 2017 Mike Jacobs
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
import pyaudio, wave, os, Queue, threading
from time import sleep

SOUND_FILE_DIR = os.path.dirname(os.path.realpath(__file__))
OPEN_SOUND_FILE_NAME = 'chimes-up.wav'
THINKING_SOUND_FILE_NAME = 'chimes-thinking.wav'
CLOSE_SOUND_FILE_NAME = 'chimes-down.wav'
DING_FILE_NAME = 'ding.wav'
FULL_OPEN_SOUND_FILE_NAME = os.path.join(SOUND_FILE_DIR, OPEN_SOUND_FILE_NAME)
FULL_THINKING_SOUND_FILE_NAME = os.path.join(SOUND_FILE_DIR, THINKING_SOUND_FILE_NAME)
FULL_CLOSE_SOUND_FILE_NAME = os.path.join(SOUND_FILE_DIR, CLOSE_SOUND_FILE_NAME)
FULL_DING_FILE_NAME = os.path.join(SOUND_FILE_DIR, DING_FILE_NAME)

AUDIO_CHUNK_SIZE = 8192
AUDIO_FORMAT = pyaudio.paInt16
AUDIO_MAXIMUM_AMPLITUDE = 32767
REFERENCE_SOUND_PRESSURE = 20e-6 # standard reference pressure of 20 micro Pascals
ADJUSTED_SPL_REFERENCE = REFERENCE_SOUND_PRESSURE * float(AUDIO_MAXIMUM_AMPLITUDE)

class RecordingListener(object):
    def onUpdate(self, data, number_of_frames):
        raise NotImplementedError("Subclasses must implement this method")


class AudioSystem(object):

    def __init__(self, volume=40, exploring=False):
        import logging
        self.__logger = logging.getLogger(__name__)

        self.__pyaudio = pyaudio.PyAudio()
        self.__queue = Queue.Queue(0)

        if(exploring):
            deviceCount = self.__pyaudio.get_device_count()
            print("--------------- D E V I C E S ----------------------------")
            for deviceIndex in range(0, deviceCount):
                deviceInfo = self.__pyaudio.get_device_info_by_index(deviceIndex)
                print("device index: {0} name: {1} inputs: {2} outputs: {3}".format(deviceIndex, deviceInfo['name'], deviceInfo['maxInputChannels'], deviceInfo['maxOutputChannels']))

            defaultAPI = self.__pyaudio.get_default_host_api_info()
            print("--------------- DEFAULT API ----------------------------")
            print(defaultAPI)
            print("--------------- APIS ----------------------------")
            apiCount = self.__pyaudio.get_host_api_count()
            for apiIndex in range(0, apiCount):
                apiInfo = self.__pyaudio.get_host_api_info_by_index(apiIndex)
                print(apiInfo)

        self.setVolume(volume)
        self.__setUpPlayThread()

    def shutdown(self):
        self.__pyaudio.terminate()

    def __recordingCallback(self, in_data, frame_count, time_info, status):
        if self.__recordingListener is not None:
            self.__recordingListener.onUpdate(in_data, frame_count)
        return (in_data, pyaudio.paContinue)

    def startRecording(self, bitrate=0, listener=None):
        self.__recordingListener = listener
        callback = None
        if listener is not None:
            callback = self.__recordingCallback
        sampleRate = bitrate
        if sampleRate == 0:
            sampleRate = self.__getInputSampleRate()
        self.__recordingSteam = self.__openNewInputStream(bitrate, callback=callback)
        self.__recordingSteam.start_stream()
        return self.__recordingSteam

    def nextRecordedSoundBite(self):
        soundbite = None
        try:
            stream = self.__recordingSteam
            if stream is not None and stream.is_active():
                soundbite = stream.read(AUDIO_CHUNK_SIZE)
        except Exception as e:
            self.stopRecording()
            self.__logger.error('Failed to read the stream', exc_info=True)
            raise
        finally:
            return soundbite

    def stopRecording(self):
        self.__recordingSteam.stop_stream()
        self.__recordingSteam.close()
        self.__recordingSteam = None
        self.__recordingListener = None

    def __openNewInputStream(self, bitrate=0, callback=None):
        if bitrate==0:
            bitrate = int(self.getInputSampleRate())
        stream = self.__pyaudio.open(format=AUDIO_FORMAT,
                        channels=1,
                        rate=bitrate,
                        input=True,
                        frames_per_buffer=AUDIO_CHUNK_SIZE,
                        stream_callback=callback)
        return stream

    def setUpMicrophone(self, duration = 3.0):
        print("Sampling background noise levels")
        noise = self.__sampleMicrophoneLevel(duration)
        print("noise level is {0} dB".format(noise))
        for count in range(3, 0, -1):
            print("Get ready in {0}".format(count))
            self.playDingSound()
            sleep(1.0)

        print(" * * * RECORDING * * * ")
        print("Please say: 'Mary had a little lamb whose fleece was white as snow and everywhere that Mary went the lamb was sure to go.'")
        signal = self.__sampleMicrophoneLevel(duration*2.0)
        print("signal level is {0} dB".format(signal))
        snr = signal/noise
        print("The signal to noise ratio is {0}".format(snr))
        return snr

    def __sampleMicrophoneLevel(self, duration = 2.0):
        import audioop
        sampleWidth = self.__pyaudio.get_sample_size(AUDIO_FORMAT)
        #print("sampleWidth: {0}".format(sampleWidth))
        inputRate = self.getInputSampleRate()
        #print("inputRate: {0}".format(inputRate))
        secondsPerBuffer = AUDIO_CHUNK_SIZE*1.0/inputRate
        stream = self.__openNewInputStream()
        elapsed = 0.0
        # By convention, 1 Pa will equal an SPL of 94 dB [SPL = sound pressure level]
        #referenceSPL = self.__decibels(AUDIO_MAXIMUM_AMPLITUDE)
        #print("reference dB: {0} should be close to 94".format(referenceSPL))

        # collect background sound samples for the requested duration
        rms = 0.0
        samples = 0.0
        decibels = 0.0
        while (elapsed <= duration):
            elapsed = elapsed + secondsPerBuffer
            # root mean square measure of sound pressure
            rms = audioop.rms(stream.read(AUDIO_CHUNK_SIZE), sampleWidth)
            decibels = decibels + self.__decibels(rms)
            samples = samples + 1.0
            #print("rms: {0} dB: {1}".format(rms, decibels))
        stream.close()
        # return average decibel level
        return (decibels/samples)

    def __decibels(self, pressure):
        import math
        # Formula of dB is 20log((Sound Pressure)/(Reference Sound Pressure))
        # The logarithmic nature of the dB scale means that each 10 dB
        # increase is a ten-fold increase in acoustic power. A 20-dB
        # increase is then a 100-fold increase in power, and a 30-dB
        # increase is a 1000-fold increase in power. A ten-fold increase
        # in acoustic power does not mean that the sound is perceived as
        # being ten times louder, however.Humans perceive a 10 dB increase
        # in sound level as only a doubling of sound loudness, and a 10 dB
        # decrease in sound level as a halving of sound loudness.
        # By convention, 1 Pa will equal an SPL of 94 dB [SPL = sound pressure level]
        spl = 20.0 * math.log10(pressure/ADJUSTED_SPL_REFERENCE)
        return spl

    def __setUpPlayThread(self):
        self.__playThread = threading.Thread(target=self.__playFromQueue, name="Inmoov audio player")
        self.__playThread.setDaemon(True)
        self.__playThread.start()

    def __playFromQueue(self):
        queue = self.__queue
        while True:
            entry = queue.get(block=True, timeout=None)
            soundFileName = entry['soundFileName']
            self.__playWaveBlocked(soundFileName)
            queue.task_done()
            sleep(0) # equivalent to yielding the thread

    def setVolume(self, aPercentage):
        newVolume = aPercentage
        if aPercentage < 0.0:
            newVolume = 0.0
        elif aPercentage > 100.0:
            newVolume = 100.0

        outputDeviceInfo = self.__pyaudio.get_default_output_device_info()
        apiIndex = outputDeviceInfo['hostApi']
        apiInfo = self.__pyaudio.get_host_api_info_by_index(apiIndex)
        if(apiInfo['name'] == 'ALSA'):
            aSetting = "{0}%".format(newVolume)
            from subprocess import call
            call(["amixer", "set", "Master", aSetting])

    def getInputSampleRate(self):
        info = self.getInputDeviceInfo()
        rate = 0.0
        if(info is not None):
            rate = info['defaultSampleRate']
        return rate

    def getInputDeviceInfo(self):
        try:
            self.__inputDeviceInfo = self.__pyaudio.get_default_input_device_info()
        except:
            self.__inputDeviceInfo = None

        return self.__inputDeviceInfo

    def getOutputDeviceInfo(self):
        try:
            self.__outputDeviceInfo = self.__pyaudio.get_default_output_device_info()
        except:
            self.__outputDeviceInfo = None

        return self.__outputDeviceInfo

    def playOpeningSound(self, block=True):
        self.playWave(FULL_OPEN_SOUND_FILE_NAME, block=block)

    def playThinkingSound(self, block=True):
        self.playWave(FULL_THINKING_SOUND_FILE_NAME, block=block)

    def playClosingSound(self, block=True):
        self.playWave(FULL_CLOSE_SOUND_FILE_NAME, block=block)

    def playDingSound(self, block=True):
        self.playWave(FULL_DING_FILE_NAME, block=block)

    def playWave(self, soundFileName, block=True):
        if block:
            self.__playWaveBlocked(soundFileName)
        else:
            self.__queue.put({'soundFileName': soundFileName}, block=False)

    def __playWaveBlocked(self, soundFileName):
        soundFile = wave.open(soundFileName,"rb")
        fileFormat = self.__pyaudio.get_format_from_width(soundFile.getsampwidth())
        stream = self.__pyaudio.open(format = fileFormat,
            channels = soundFile.getnchannels(),
            rate = soundFile.getframerate(),
            output = True)
        data = soundFile.readframes(AUDIO_CHUNK_SIZE)
        #play stream
        stream.start_stream()
        while data:
            stream.write(data)
            data = soundFile.readframes(AUDIO_CHUNK_SIZE)
        #stop stream
        stream.stop_stream()
        stream.close()

    def waitUntilDone(self, timeout=7.0):
        self.__queue.join()

if __name__ == "__main__":

    try:
        s = AudioSystem(exploring=False, volume=37)
        # outputs
        outputInfo = s.getOutputDeviceInfo()
        print(outputInfo)
        print("Playing sounds ...")
        s.playOpeningSound()
        s.playThinkingSound()
        s.playClosingSound()
        s.waitUntilDone()

        # input
        print("Testing microphone levels")
        s.setUpMicrophone()

        s.shutdown()
    except:
        print "exception occurred!"
        import sys, traceback
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exception(exc_type, exc_value, exc_traceback,
                              limit=5, file=sys.stdout)

    print "done"
