#!/usr/bin/python

# Copyright (c) 2016 Mike Jacobs
# 
# Permission is hereby granted, free of charge, to any person obtaining 
# a copy of this software and associated documentation files 
# (the "Software"), to deal in the Software without restriction, 
# including without limitation the rights to use, copy, modify, merge, 
# publish, distribute, sublicense, and/or sell copies of the Software, 
# and to permit persons to whom the Software is furnished to do so, 
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 

import rospy

import actionlib
from inmoov.msg import HandAction, HandGoal, HandResult
from Hand import Hand

SUCCESS = 0
CANCELLED = 1
FAILED = 2
NODE_NAME = 'Hand'

def handleGoal(goal):
    result = HandResult()
    result.returnCode = SUCCESS
    hand.setWristAngle(goal.wrist)
    hand.setThumbAngle(goal.thumb)
    hand.setIndexAngle(goal.index)
    hand.setMiddleAngle(goal.middle)
    hand.setRingAngle(goal.ring)
    hand.setPinkyAngle(goal.pinky)
    server.set_succeeded(result)
   

def onShutdown():
    rospy.logdebug("Hand Action Server shutting down")
    hand.rest()
    rospy.sleep(5.0)

rospy.init_node('InMoov_Hand_Action_Server')
rospy.on_shutdown(onShutdown)
namespace = rospy.get_namespace()
hand = Hand(namespace, debug=False)
hand.rest() 
rospy.logdebug("Creating hand action server {0}".format(namespace))
server = actionlib.SimpleActionServer(NODE_NAME, HandAction, handleGoal, False)
rospy.logdebug("Starting hand action server {0}".format(namespace))
server.start()
rospy.logdebug("Hand server SUCCESSFULLY started".format(namespace))
try:
    rospy.spin()
except:
    rospy.logerror("Hand action server {0} FAILED".format(namespace))
    hand.rest()

