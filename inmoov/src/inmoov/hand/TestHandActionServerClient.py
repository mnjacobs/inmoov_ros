#!/usr/bin/python

# Copyright (c) 2016 Mike Jacobs
# 
# Permission is hereby granted, free of charge, to any person obtaining 
# a copy of this software and associated documentation files 
# (the "Software"), to deal in the Software without restriction, 
# including without limitation the rights to use, copy, modify, merge, 
# publish, distribute, sublicense, and/or sell copies of the Software, 
# and to permit persons to whom the Software is furnished to do so, 
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
import sys
import rospy
import actionlib

from inmoov.msg import HandAction, HandGoal, HandResult

def onShutdown():
    rospy.loginfo("Shutting down")

try:  
    rospy.init_node('test_hand_action_server', log_level=rospy.DEBUG)
    rospy.on_shutdown(onShutdown)
    handActionClient = actionlib.SimpleActionClient('inmoov_hand', HandAction)
    rospy.loginfo("Waiting for hand action server...")
    finished = handActionClient.wait_for_server(timeout = rospy.Duration(3.0))
    if(finished is False): rospy.logerr("Hand action server timed out")
    else:
        rospy.loginfo("Hand action server found")
        goal = HandGoal()
        goal.index = 90
        rospy.loginfo("Sending goal ...")
        handActionClient.send_goal(goal)
        rospy.loginfo("waiting for result ...")
        finished = handActionClient.wait_for_result()
        rospy.sleep(1)
        goal.index = 0
        rospy.loginfo("Sending goal ...")
        handActionClient.send_goal(goal)
        rospy.loginfo("waiting for result ...")
        handActionClient.wait_for_result()
        rospy.loginfo("Done")
    
except:
    print("An exception was encountered")
    e = sys.exc_info()[0]
    print str(e)
    
    

