#!/usr/bin/python

# Copyright (c) 2016 Mike Jacobs
# 
# Permission is hereby granted, free of charge, to any person obtaining 
# a copy of this software and associated documentation files 
# (the "Software"), to deal in the Software without restriction, 
# including without limitation the rights to use, copy, modify, merge, 
# publish, distribute, sublicense, and/or sell copies of the Software, 
# and to permit persons to whom the Software is furnished to do so, 
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 

""" 
This module represents a hand including the wrist
"""
import rospy
import math
from servo.Servo import ServoController, PowerHD1501MGServo
import time

# The two gears in the wrist require a mapping from the real world
# desired angle to the servo angle to request.  The ratio is based on
# the number of teeth around the gears.
WRIST_GEAR_RATIO = 20.0/11.0

# Collect the configuration parameters for this hand
import sys, traceback
try: 
    hand_parameters = rospy.get_param('Hand')
    WRIST_MIN = hand_parameters['wrist_min']
    WRIST_MAX = hand_parameters['wrist_max']
    
    THUMB_MIN_WMIN = hand_parameters['thumb_min_wmin'] 
    THUMB_MAX_WMIN = hand_parameters['thumb_max_wmin']
    THUMB_MIN_WMAX = hand_parameters['thumb_min_wmax']
    THUMB_MAX_WMAX = hand_parameters['thumb_max_wmax']
        
    INDEX_MIN_WMIN = hand_parameters['index_min_wmin']
    INDEX_MAX_WMIN = hand_parameters['index_max_wmin']
    INDEX_MIN_WMAX = hand_parameters['index_min_wmax']
    INDEX_MAX_WMAX = hand_parameters['index_max_wmax']
        
    MIDDLE_MIN_WMIN = hand_parameters['middle_min_wmin']
    MIDDLE_MAX_WMIN = hand_parameters['middle_max_wmin']
    MIDDLE_MIN_WMAX = hand_parameters['middle_min_wmax']
    MIDDLE_MAX_WMAX = hand_parameters['middle_max_wmax']
        
    RING_MIN_WMIN = hand_parameters['ring_min_wmin']
    RING_MAX_WMIN = hand_parameters['ring_max_wmin']
    RING_MIN_WMAX = hand_parameters['ring_min_wmax']
    RING_MAX_WMAX = hand_parameters['ring_max_wmax']
        
    PINKY_MIN_WMIN = hand_parameters['pinky_min_wmin']
    PINKY_MAX_WMIN = hand_parameters['pinky_max_wmin']
    PINKY_MIN_WMAX = hand_parameters['pinky_min_wmax']
    PINKY_MAX_WMAX = hand_parameters['pinky_max_wmax']
       
except KeyError:
    print "Configuration parameter cannot be found"
    exc_type, exc_value, exc_traceback = sys.exc_info()
    traceback.print_exception(exc_type, exc_value, exc_traceback,
                              limit=5, file=sys.stdout)


class Range(object):
    def __init__(self, keyValue, minValue, maxValue):
        self.__keyValue = keyValue
        self.__minValue = minValue
        self.__maxValue = maxValue
    
    def getKeyValue(self):
        return self.__keyValue
        
    def getMinValue(self):
        return self.__minValue
    
    def getMaxValue(self):
        return self.__maxValue
    
    

class Finger(object):
    def __init__(self, name, controller, port, debug=False):
        self.__name = name
        self.__servo = PowerHD1501MGServo(controller, port, debug)
        self.__servo.setName(name)
        self.debug = debug
    
    def setAngle(self, degrees):
        # No load speed is 0.16 seconds / 60 degrees (375 degrees/sec)
        #currentAngle = self.__servo.getAngle()
        #distance = 1.0*math.fabs(degrees - currentAngle)
        #duration = distance/375.0
        self.__servo.setAngle(degrees)
        
    
    def setRanges(self, minWristRange, maxWristRange):
        self._minWristRange = minWristRange
        self._maxWristRange = maxWristRange
    
    def __lerp(self, v0, v1, t) :
        """ Private: perform precise linear interpolation of t between v0 and v1 """
        lerp = (1.0-t)*v0 + t*v1
        return lerp
        
    def __getAdjustedRange(self, aWristAngle):
        # linearly interpret the min and max range based on the wrist angle
        minWristValue = self._minWristRange.getKeyValue()
        maxWristValue = self._maxWristRange.getKeyValue()
        wristRange = maxWristValue - minWristValue
        angleRatio = 1.0*(aWristAngle - minWristValue)/wristRange
        minValue = int(round(self.__lerp(self._minWristRange.getMinValue(), self._maxWristRange.getMinValue(), angleRatio)))
        maxValue = int(round(self.__lerp(self._minWristRange.getMaxValue(), self._maxWristRange.getMaxValue(), angleRatio)))
        if(self.debug) :
            print('[{2}] getAdjustedRange min:{0}, max:{1}'.format(minValue, maxValue, self.__name))
        return minValue, maxValue
        
    def constrainRange(self, aWristAngle):
        minValue, maxValue = self.__getAdjustedRange(aWristAngle)
        angle = self.__servo.getAngle()
        self.__servo.constrainAngles(minValue, maxValue)
        self.__servo.setAngle(angle)

class Hand(object):
    
    def __init__(self, name, i2cAddress=0x40, frequency=50, thumbPort=0, indexPort=1, middlePort=2, ringPort=3, pinkyPort=4, wristPort=5, debug=False):
        self.name = name
        
        controller = ServoController(i2cAddress, frequency, debug)
        
        self._thumb = Finger("thumb", controller, thumbPort, debug)
        self._thumb.setRanges(Range(WRIST_MIN,THUMB_MIN_WMIN,THUMB_MAX_WMIN), 
                              Range(WRIST_MAX,THUMB_MIN_WMAX,THUMB_MAX_WMAX))
        
        self._index = Finger("index", controller, indexPort, debug)
        self._index.setRanges(Range(WRIST_MIN,INDEX_MIN_WMIN,INDEX_MAX_WMIN), 
                              Range(WRIST_MAX,INDEX_MIN_WMAX,INDEX_MAX_WMAX))
        
        self._middle = Finger("middle", controller, middlePort, debug)
        self._middle.setRanges(Range(WRIST_MIN,MIDDLE_MIN_WMIN,MIDDLE_MAX_WMIN), 
                               Range(WRIST_MAX,MIDDLE_MIN_WMAX,MIDDLE_MAX_WMAX))
        
        self._ring = Finger("ring", controller, ringPort, debug)
        self._ring.setRanges(Range(WRIST_MIN,RING_MIN_WMIN,RING_MAX_WMIN), 
                             Range(WRIST_MAX,RING_MIN_WMAX,RING_MAX_WMAX))
        
        self._pinky = Finger("pinky", controller, pinkyPort, debug)
        self._pinky.setRanges(Range(WRIST_MIN,PINKY_MIN_WMIN,PINKY_MAX_WMIN), 
                              Range(WRIST_MAX,PINKY_MIN_WMAX,PINKY_MAX_WMAX))
        
        self._wrist = PowerHD1501MGServo(controller, wristPort, debug)
        self._wrist.setName("wrist")
        self._wrist.constrainAngles(WRIST_MIN, WRIST_MAX)

    
    def setWristAngle(self, requested, adjust=True):
        # The wrist is geared at WRIST_GEAR_RATIO ratio so the requested 
        # angle needs to be adjusted to map to the correct servo angle
        if(adjust == True): value = int(round(WRIST_GEAR_RATIO * requested))
        else: value = requested
        # The servo value may exceed the wrist constraints, so adjust it
        adjusted = self._wrist.getConstrainAdjustedValue(value)
        # adjust finger constraints based on new wrist position
        # rospy.loginfo("Wrist angle requested: {0}, adjusted servo angle: {1}".format(requested, adjusted))
        self._thumb.constrainRange(adjusted)
        self._index.constrainRange(adjusted)
        self._middle.constrainRange(adjusted)
        self._ring.constrainRange(adjusted)
        self._pinky.constrainRange(adjusted)
        self._wrist.setAngle(adjusted)
    
    def setThumbAngle(self, value):
        self._thumb.setAngle(value)
        
    def setIndexAngle(self, value):
        self._index.setAngle(value)
    
    def setMiddleAngle(self, value):
        self._middle.setAngle(value)
    
    def setRingAngle(self, value):
        self._ring.setAngle(value)
        
    def setPinkyAngle(self, value):
        self._pinky.setAngle(value)
    
    def rest(self):
        self.setWristAngle(0)
        self.setThumbAngle(0)
        self.setIndexAngle(0)
        self.setMiddleAngle(0)
        self.setRingAngle(0)
        self.setPinkyAngle(0)
    
if __name__ == "__main__":
    import sys, traceback
    # Test code  
    debugging = False
    step = 1
    try: 
        rightHand = Hand('right')
        if(step == 1):
            # WARNING: BE SURE ALL FISHING LINE IS DETACHED FOR THIS STEP
            rightHand.setThumbAngle(30)
            rightHand.setIndexAngle(30)
            rightHand.setMiddleAngle(30)
            rightHand.setRingAngle(30)
            rightHand.setPinkyAngle(30)
            rightHand.setWristAngle(90)
            print "Attach all fishing lines to servos now" 
        
          
       
    except:
        print "exception occurred!"
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exception(exc_type, exc_value, exc_traceback,
                              limit=5, file=sys.stdout)
    
    print "done" 
    
    
        
        
        
