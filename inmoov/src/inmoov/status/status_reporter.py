#! /usr/bin/env python

# Copyright (c) 2017 Mike Jacobs
# 
# Permission is hereby granted, free of charge, to any person obtaining 
# a copy of this software and associated documentation files 
# (the "Software"), to deal in the Software without restriction, 
# including without limitation the rights to use, copy, modify, merge, 
# publish, distribute, sublicense, and/or sell copies of the Software, 
# and to permit persons to whom the Software is furnished to do so, 
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 

import rospy
from inmoov.msg import Status

IMPROMPTU_STATUS_REPORTING_TOPIC = 'impromptu_status'  
STATUS_REPORTING_QUEUE_SIZE = 2

class StatusReporter(object):
        
    def __init__(self, component):
        self.__component = component
        self.__publisher = rospy.Publisher(IMPROMPTU_STATUS_REPORTING_TOPIC, Status, queue_size=STATUS_REPORTING_QUEUE_SIZE)
        
    def info(self, status, message):
        event = Status(self.__component, status, Status.SEVERITY_INFO, message)
        self.__publisher.publish(event)
    
    def warning(self, status, message):
        event = Status(self.__component, status, Status.SEVERITY_WARNING, message)
        self.__publisher.publish(event)
        
    def error(self, status, message):
        event = Status(self.__component, status, Status.SEVERITY_ERROR, message)
        self.__publisher.publish(event)
        
    def fatal(self, status, message):
        event = Status(self.__component, status, Status.SEVERITY_FATAL, message)
        self.__publisher.publish(event)
        
    def __shutdown(self):
        self.__publisher.unregister()

