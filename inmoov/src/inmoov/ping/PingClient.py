#!/usr/bin/python

# Copyright (c) 2016 Mike Jacobs
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#  
import sys
import rospy
import actionlib

from inmoov.msg import PingAction, PingGoal, PingResult

def onShutdown():
    rospy.loginfo("Shutting down")

try:
    rospy.init_node('ping_client', log_level=rospy.DEBUG)
    rospy.on_shutdown(onShutdown)
    client = actionlib.SimpleActionClient('ping', PingAction)
    rospy.loginfo("Waiting for ping action server...")
    finished = client.wait_for_server(timeout = rospy.Duration(10.0))
    if(finished is False):
        rospy.logerr("Ping action server timed out")
    else:
        rospy.loginfo("Ping action server found")

except:
    print("An exception was encountered")
    e = sys.exc_info()[0]
    print str(e)
