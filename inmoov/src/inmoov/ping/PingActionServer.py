#!/usr/bin/python

# Copyright (c) 2016 Mike Jacobs
# 
# Permission is hereby granted, free of charge, to any person obtaining 
# a copy of this software and associated documentation files 
# (the "Software"), to deal in the Software without restriction, 
# including without limitation the rights to use, copy, modify, merge, 
# publish, distribute, sublicense, and/or sell copies of the Software, 
# and to permit persons to whom the Software is furnished to do so, 
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 

import rospy
import actionlib
from inmoov.msg import PingAction, PingGoal, PingResult

SUCCESS = 0
CANCELLED = 1
FAILED = 2

def handleGoal(goal):
    result = PingResult()
    result.returnCode = SUCCESS
    print(goal.message)
    server.set_succeeded(result)
   

def onShutdown():
    rospy.loginfo("Ping Action Server shutting down")

rospy.init_node('ping_action_server')
rospy.on_shutdown(onShutdown)
rospy.loginfo("Creating Ping action sever")
server = actionlib.SimpleActionServer('ping', PingAction, handleGoal, False)
rospy.loginfo("Staring Ping action server")
server.start()
rospy.loginfo("Entering ROS Python spin loop for Ping server")
try:
    rospy.spin()
except:
    rospy.loginfo("Ping Action Server encountered an exception")

